from django.db import models

class Page(models.Model):
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug', blank=True, null=True, unique=True)
    Image = models.ImageField('image', upload_to='page')
    text = models.TextField('text')

    def __str__(self):
        return self.title

class PageItem(models.Model):
    Page = models.ForeignKey(Page, related_name='page_page_items', on_delete=models.CASCADE)
    Image = models.ImageField('image', upload_to='page_item')
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug', blank=True, null=True, unique=True)
    publish = models.BooleanField('publish', default=True)

    def get_absolute_url(self):
        return '/catalog/%s/%s/' % (self.Page.slug,  self.slug)

    def __str__(self):
        return self.Page.title

class Product(models.Model):
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug')
    items = models.ForeignKey(PageItem, related_name='subcategory_products', on_delete=models.CASCADE, blank=True, null=True)
    price = models.CharField('price', max_length=255)
    Images = models.ImageField('images', upload_to='products')
    publish = models.BooleanField('publish', default=True)

    def __str__(self):
        return self.title

class PriceType(models.Model):
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug')
    product = models.ForeignKey(Product, related_name='price_type_products', on_delete=models.CASCADE, blank=True, null=True)
    publish = models.BooleanField('publish', default=True)

    def __str__(self):
        return self.title


# class Price(models.Model):
#     product = models.ForeignKey(Product, related_name='product_prices', on_delete=models.CASCADE)
#     price_type = models.ForeignKey(PriceType, related_name='price_type_prices', on_delete=models.CASCADE, blank=True, null=True)
#     price = models.FloatField('price')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.product.title

