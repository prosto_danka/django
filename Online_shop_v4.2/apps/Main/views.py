from django.shortcuts import render

from apps.Catalog.models import Category
from apps.Main.models import *
from django.http import Http404

def products(request, category_slug, subcategory_slug):
    context = {}

    if Category.objects.filter(publish=True, slug=category_slug).exists():
        category = Category.objects.get(publish=True, slug=category_slug)
        context['category'] = category
    else:
        raise Http404

    if PageItem.objects.filter(publish=True, slug=subcategory_slug).exists():
        category = PageItem.objects.get(publish=True, slug=subcategory_slug)
        context['PageItem'] = category
    else:
        raise Http404

    products = Product.objects.filter(publish=True, items__slug=subcategory_slug)
    context['products'] = products

    return render(request, 'catalog/products.html', context)


def index(request):
    return render(request, 'index.html')

def contact(request):
    return render(request, 'contact.html')

def laptops(request):
    context = {}

    if Page.objects.filter(slug='laptops').exists():
        page = Page.objects.get(slug='laptops')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def tablets(request):
    context = {}

    if Page.objects.filter(slug='tablets').exists():
        page = Page.objects.get(slug='tablets')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def smartphone(request):
    context = {}

    if Page.objects.filter(slug='smartphone').exists():
        page = Page.objects.get(slug='smartphone')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def cameras(request):
    context = {}

    if Page.objects.filter(slug='cameras').exists():
        page = Page.objects.get(slug='cameras')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def headphones(request):
    context = {}

    if Page.objects.filter(slug='headphones').exists():
        page = Page.objects.get(slug='headphones')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)