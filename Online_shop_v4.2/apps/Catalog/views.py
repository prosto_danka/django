from django.shortcuts import render
from apps.Catalog.models import Category
from django.http import Http404







def mainCategories(request):
    context = {}
    categories = Category.objects.filter(publish=True)
    context['categories'] = categories
    return render(request, 'catalog/mainCategories.html', context)

def subcategories(request, slug):
    context = {}

    if Category.objects.filter(publish=True, slug=slug).exists():
        category = Category.objects.get(publish=True, slug=slug)
        context['category'] = category
        context['subcategory'] = category.subcategories.filter(publish=True)

    else:
        raise Http404
    return render(request, 'catalog/categories.html', context)

