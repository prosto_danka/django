from django.contrib import admin
from django.urls import path, include
from apps.Catalog import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('catalog/', views.mainCategories, name="mainCategories"),
    path('catalog/<str:slug>/', views.subcategories, name="subcategories"),

]

