from django.contrib import admin
from apps.Catalog.models import *


# class SubcategoryInLine(admin.TabularInline):
#     model = Subcategory
#     extra = 0
#     prepopulated_fields = {"slug": ("title",)}

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]
    # inlines = [SubcategoryInLine, ]


# @admin.register(Collection)
# class CollectionAdmin(admin.ModelAdmin):
#     prepopulated_fields = {"slug": ("title",)}
#     list_display = ['title', 'slug', 'publish', ]

# @admin.register(Material)
# class MaterialAdmin(admin.ModelAdmin):
#     prepopulated_fields = {"slug": ("title",)}
#     list_display = ['title', 'slug', 'publish', ]

# @admin.register(Color)
# class ColorAdmin(admin.ModelAdmin):
#     prepopulated_fields = {"slug": ("title",)}
#     list_display = ['title', 'slug', 'publish', ]

# @admin.register(PriceType)
# class PriceTypeAdmin(admin.ModelAdmin):
#     prepopulated_fields = {"slug": ("title",)}
#     list_display = ['title', 'slug', 'publish', ]
#
#
# class PriceInline(admin.TabularInline):
#     model = Price
#     extra = 0


    # filter_horizontal = ['colors', ]