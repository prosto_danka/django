from django.db import models

from apps.Main.models import PageItem, Page


class Category(models.Model):
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug')
    text = models.TextField('text')
    image = models.ImageField('image', upload_to='categories', blank=True, null=True)
    publish = models.BooleanField('publish', default=True)

    def get_absolute_url(self):
        return '/catalog/%s/' % self.slug

    def __str__(self):
        return self.title

class Subcategory(models.Model):
    category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug')
    publish = models.BooleanField('publish', default=True)

    def get_absolute_url(self):
        return '/catalog/%s/%s/' % (self.category.slug,  self.slug)

    def __str__(self):
        return self.title

# class Collection(models.Model):
#     title = models.CharField('title', max_length=255)
#     slug = models.SlugField('slug')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.title


# class Material(models.Model):
#     title = models.CharField('title', max_length=255)
#     slug = models.SlugField('slug')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.title
#
#
# class Color(models.Model):
#     title = models.CharField('title', max_length=255)
#     slug = models.SlugField('slug')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.title




# def cover_image(self):
#     if self.product_images.exists():
#         return self.product_images.first().image

# class ProductImage(models.Model):
#     product = models.ForeignKey(PageItem, related_name='product_images', on_delete=models.CASCADE)
#     image = models.ImageField('image', upload_to='product')




# class PriceType(models.Model):
#     title = models.CharField('title', max_length=255)
#     slug = models.SlugField('slug')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.title
#
#
# class Price(models.Model):
#     product = models.ForeignKey(Product, related_name='product_prices', on_delete=models.CASCADE)
#     price_type = models.ForeignKey(PriceType, related_name='price_type_prices', on_delete=models.CASCADE, blank=True, null=True)
#     price = models.FloatField('price')
#     publish = models.BooleanField('publish', default=True)
#
#     def __str__(self):
#         return self.product.title