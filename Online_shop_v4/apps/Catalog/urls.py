from django.contrib import admin
from django.urls import path, include
from apps.Catalog import views
from django.conf.urls.static import static
from django.conf import settings
# import tinymce

urlpatterns = [
    path('catalog/', views.mainCategories, name="mainCategories"),
    path('catalog/<str:slug>/', views.subcategories, name="subcategories"),
    path('catalog/<str:category_slug>/<str:subcategory_slug>/', views.products, name="products"),
]

