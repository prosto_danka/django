from django.shortcuts import render
from apps.Catalog.models import Category, Product
from django.http import Http404

from apps.Main.models import PageItem






def mainCategories(request):
    context = {}
    categories = Category.objects.filter(publish=True)
    context['categories'] = categories
    return render(request, 'catalog/mainCategories.html', context)

def subcategories(request, slug):
    context = {}

    if Category.objects.filter(publish=True, slug=slug).exists():
        category = Category.objects.get(publish=True, slug=slug)
        context['category'] = category
        context['subcategory'] = category.subcategories.filter(publish=True)

    else:
        raise Http404
    return render(request, 'catalog/categories.html', context)

def products(request, category_slug, subcategory_slug):
    context = {}

    if Category.objects.filter(publish=True, slug=category_slug).exists():
        category = Category.objects.get(publish=True, slug=category_slug)
        context['category'] = category
    else:
        raise Http404

    if PageItem.objects.filter(publish=True, slug=subcategory_slug).exists():
        category = PageItem.objects.get(publish=True, slug=subcategory_slug)
        context['PageItem'] = category
    else:
        raise Http404

    products = Product.objects.filter(publish=True, items__slug=subcategory_slug)
    context['products'] = products

    return render(request, 'catalog/products.html', context)