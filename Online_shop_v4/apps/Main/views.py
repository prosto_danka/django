from django.shortcuts import render
from apps.Main.models import Page
from django.http import Http404

def index(request):
    return render(request, 'index.html')

def contact(request):
    return render(request, 'contact.html')

def laptops(request):
    context = {}

    if Page.objects.filter(slug='laptops').exists():
        page = Page.objects.get(slug='laptops')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def tablets(request):
    context = {}

    if Page.objects.filter(slug='tablets').exists():
        page = Page.objects.get(slug='tablets')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def smartphone(request):
    context = {}

    if Page.objects.filter(slug='smartphone').exists():
        page = Page.objects.get(slug='smartphone')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def cameras(request):
    context = {}

    if Page.objects.filter(slug='cameras').exists():
        page = Page.objects.get(slug='cameras')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)

def headphones(request):
    context = {}

    if Page.objects.filter(slug='headphones').exists():
        page = Page.objects.get(slug='headphones')

    else:
        raise Http404

    context['page'] = page
    return render(request, 'catalog/categories.html', context)