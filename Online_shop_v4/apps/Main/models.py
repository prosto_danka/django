from django.db import models

class Page(models.Model):
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug', blank=True, null=True, unique=True)
    Image = models.ImageField('image', upload_to='page')
    text = models.TextField('text')

    def __str__(self):
        return self.title

class PageItem(models.Model):
    page = models.ForeignKey(Page, related_name='page_page_items', on_delete=models.CASCADE)
    Image = models.ImageField('image', upload_to='page_item')
    title = models.CharField('title', max_length=255)
    slug = models.SlugField('slug', blank=True, null=True, unique=True)
    # text = models.TextField('text')
    # price = models.TextField('price')
    publish = models.BooleanField('publish', default=True)

    def get_absolute_url(self):
        return '/catalog/%s/%s/' % (self.page.slug,  self.slug)

    def __str__(self):
        return self.page.title

