from django.contrib import admin

from apps.Catalog.models import ProductImage
from .models import Page, PageItem

class PageItemInLine(admin.TabularInline):
    model = PageItem
    extra = 0
    prepopulated_fields = {"slug": ("title",)}

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    inlines = [PageItemInLine,]
    list_display = ['title', 'slug', ]
    prepopulated_fields = {"slug": ("title",)}

@admin.register(PageItem)
class PageItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', ]
    prepopulated_fields = {"slug": ("title",)}
    inlines = [ProductImageInline, ]