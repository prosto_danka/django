
from django.contrib import admin
from django.urls import path, include
from apps.Main import views
from django.conf.urls.static import static
from django.conf import settings
import tinymce
from apps.Catalog.urls import urlpatterns as url_catalog

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('index/', views.index, name="index"),

    path('contact/', views.contact, name="contact"),

    path('catalog/laptops/', views.laptops, name="categories_laptops"),
    path('catalog/tablets/', views.tablets, name="categories_tablets"),
    path('catalog/smartphone/', views.smartphone, name="categories_smartphone"),
    path('catalog/cameras/', views.cameras, name="categories_cameras"),
    path('catalog/headphones/', views.headphones, name="categories_headphones"),



    path('tinymce/', include('tinymce.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += url_catalog