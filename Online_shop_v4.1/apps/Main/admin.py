from django.contrib import admin
from .models import *

class PageItemInLine(admin.TabularInline):
    model = PageItem
    extra = 0
    prepopulated_fields = {"slug": ("title",)}

class ProductInline(admin.TabularInline):
    model = Product
    extra = 0
    prepopulated_fields = {"slug": ("title",)}

@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    inlines = [PageItemInLine,]
    list_display = ['title', 'slug', ]
    prepopulated_fields = {"slug": ("title",)}

@admin.register(PageItem)
class PageItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', ]
    prepopulated_fields = {"slug": ("title",)}
    inlines = [ProductInline, ]

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'items', 'price', 'publish', ]